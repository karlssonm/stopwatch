//
//  ViewController.m
//  stopWatch
//
//  Created by Mattias Karlsson on 2015-04-19.
//  Copyright (c) 2015 Mattias Karlsson. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *minutesLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondsLabel;
@property (weak, nonatomic) IBOutlet UIButton *startButton;
@property (nonatomic) NSTimer *timer;
@property (nonatomic) bool running;
@property (nonatomic) int minutes;
@property (nonatomic) int seconds;


@end

@implementation ViewController
- (IBAction)start:(id)sender {
    
    if (!self.running) {
        [self startTimer];
        [sender setTitle:@"Pause" forState:UIControlStateNormal];
        self.running = YES;
    }
    else {
        [sender setTitle:@"Start" forState:UIControlStateNormal];
        self.running = NO;
        [self.timer invalidate];
    }
    
}

- (IBAction)stop:(id)sender {
    [self stopTimer];
    [self.startButton setTitle:@"Start" forState:UIControlStateNormal];
    self.running = NO;
}
-(void)startTimer {
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(update) userInfo:nil repeats:YES];
}
-(void)stopTimer {
    [self.timer invalidate];
    self.timer = nil;
    [self reset];
    
}
-(void)update {
    self.seconds ++;
    if (self.seconds>59) {
        self.seconds = 0;
        self.minutes ++;
    }
    self.minutesLabel.text = [NSString stringWithFormat:@"%d",self.minutes];
    self.secondsLabel.text = [NSString stringWithFormat:@"%d",self.seconds];
}
-(void)reset {
    self.minutes = 0;
    self.seconds = 0;
    self.secondsLabel.text = @"0";
    self.minutesLabel.text = @"0";
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self reset];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
